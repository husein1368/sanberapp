import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { createDrawerNavigator } from "@react-navigation/drawer";
import LoginScreen from "./LoginScreen";
import SkillScreen from "./SkillScreen";
import ProjectScreen from "./ProjectScreen";
import AddScreen from "./AddScreen";
import AboutScreen from "./AboutScreen";

const RootStack = createStackNavigator();
const Drawer = createDrawerNavigator();
const Tabs = createBottomTabNavigator();

const TabsScreen = () => (
  <Tabs.Navigator>
    <Tabs.Screen name="Home" component={SkillScreen} />
    <Tabs.Screen name="Profile" component={ProjectScreen} />
    <Tabs.Screen name="Search" component={AddScreen} />
  </Tabs.Navigator>
)

const DrawerScreen = () => (
  <Drawer.Navigator>
    <Drawer.Screen name='Home' component={TabsScreen} />
    <Drawer.Screen name='About' component={AboutScreen} />
  </Drawer.Navigator>
)
export default () => {
  return (
    <NavigationContainer>
      <RootStack.Navigator>
        <RootStack.Screen name="LoginScreen" component={LoginScreen} />
        <RootStack.Screen name="Drawer" component={DrawerScreen} />
      </RootStack.Navigator>
    </NavigationContainer >
  );
}