import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    KeyboardAvoidingView
} from 'react-native';
import MyInput from './components/MyInput';
import MyButton from './components/MyButton';

export default class Register extends Component {
    render() {
        return (
            <KeyboardAvoidingView style={styles.container} behavior="padding">
                <StatusBar style="auto" />
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('./images/logo.png')} style={styles.logo} />
                    <Text style={styles.title}>Register</Text>
                </View>
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <MyInput title='Username' />
                    <MyInput title='Email' />
                    <MyInput title='Password' />
                    <MyInput title='Ulangi Password' />
                </View>
                <MyButton title='Daftar' bgColor='#003366' color='#FFFFFF' />
                <Text style={{ fontSize: 24, color: '#3EC6FF' }}>atau</Text>
                <MyButton title='Masuk ?' bgColor='#3EC6FF' color='#FFFFFF' />
            </KeyboardAvoidingView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "space-between"
    },
    logo: {
        marginTop: 63
    },
    title: {
        fontSize: 24,
        color: '#003366',
        margin: 20,
        padding: 20
    },
});