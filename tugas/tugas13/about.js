import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
    SafeAreaView, 
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import FaIcon from 'react-native-vector-icons/FontAwesome';

export default class Register extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Text style={{ fontSize: 36, fontWeight: '700', color: '#003366' }}>Tentang Saya</Text>
                <View style={{ width: 200, height: 200, borderRadius: 200 / 2, justifyContent: 'center', alignItems: 'center', backgroundColor: '#EFEFEF' }}>
                    <Icon name="person" size={195} style={{ color: '#CACACA' }} />
                </View>
                <Text style={{ fontSize: 24, fontWeight: '700', color: '#003366' }}>Mukhlis hanafi</Text>
                <Text style={{ fontSize: 16, fontWeight: '700', color: '#3EC6FF' }}>React Native Developer</Text>
                <View style={{ height: 140, width: 359, backgroundColor: '#EFEFEF', alignItems: 'center', borderRadius:15 }}>
                    <View style={{ width: 350, borderBottomWidth: 1 }}>
                        <Text style={styles.title}>Portofolio</Text>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around', width: 359, height:106 }}>
                        <View style={{justifyContent:'center', alignItems:'center'}}>
                            <FaIcon name="gitlab" size={50} style={{color:'#3EC6FF'}}/>
                            <Text style={styles.title}>@mukhlish</Text>
                        </View>
                        <View style={{justifyContent:'center', alignItems:'center'}}>
                            <FaIcon name="github" size={50} style={{color:'#3EC6FF'}}/>
                            <Text style={styles.title}>@mukhlis-h</Text>
                        </View>
                    </View>
                </View>
                <View style={{ height: 251, width: 359, backgroundColor: '#EFEFEF', alignItems: 'center', borderRadius:15 }}>
                    <View style={{ width: 350, borderBottomWidth: 1 }}>
                        <Text style={styles.title}>Hubungi Saya</Text>
                    </View>
                    <View style={{ flexDirection: 'column', alignItems: 'center', justifyContent: 'space-around', width: 359, height:231 }}>
                        <View style={{flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                            <FaIcon name="facebook-square" size={50} style={{color:'#3EC6FF'}}/>
                            <Text style={styles.title}>mukhlis.hanafi</Text>
                        </View>
                        <View style={{flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                            <FaIcon name="instagram" size={50} style={{color:'#3EC6FF'}}/>
                            <Text style={styles.title}>@mukhlis_hanafi</Text>
                        </View>
                        <View style={{flexDirection: 'row', justifyContent:'center', alignItems:'center'}}>
                            <FaIcon name="twitter" size={50} style={{color:'#3EC6FF'}}/>
                            <Text style={styles.title}>@mukhlis</Text>
                        </View>
                    </View>
                </View>
            </View>
        )
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "space-around",
    },
    logo: {
        marginTop: 63
    },
    title: {
        fontSize: 16,
        fontWeight: '700',
        color: '#003366',
        margin: 5,
    },
});