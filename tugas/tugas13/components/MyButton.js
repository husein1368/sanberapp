import React, { Component } from 'react';
import {
    Platform,
    Text,
    View,
    StyleSheet,
    TouchableOpacity
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';


export default class MyButton extends Component {
    render() {
        let title = this.props.title;
        let bgColor = this.props.bgColor;
        let color = this.props.color;
        return (
            <View style={styles.container}>
                <TouchableOpacity style={[{backgroundColor:bgColor}, styles.button]}>
                    <Text style={[{color:color}, styles.btnTitle]}>{title}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        margin: 5,
        padding: 5
    },
    button:{
        borderRadius:16,
        height:40,
        width:140,
        justifyContent:'center',
        alignItems:'center'
    },
    btnTitle:{
        fontSize:24
    }
});