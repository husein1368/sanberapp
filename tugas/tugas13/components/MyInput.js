import React, { Component } from 'react';
import {
    Platform,
    Text,
    View,
    TextInput,
    StyleSheet,
} from 'react-native';

import Icon from 'react-native-vector-icons/MaterialIcons';


export default class MyInput extends Component {
    render() {
        let title = this.props.title;
        return (
            <View style={styles.container}>
                <Text>{title}</Text>
                <TextInput
                    style={styles.input}
                />
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        marginTop: 5,
        padding: 5
    },
    input: {
        width: 294,
        height: 48,
        backgroundColor: '#FFF',
        borderColor: '#003366',
        borderWidth: 1
    }
});