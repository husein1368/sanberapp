import { StatusBar } from 'expo-status-bar';
import React, { Component } from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    Image,
} from 'react-native';
import MyInput from './components/MyInput';
import MyButton from './components/MyButton';

export default class Register extends Component {
    render() {
        return (
            <View style={styles.container} behavior="padding">
                <StatusBar style="auto" />
                <View style={{ justifyContent: 'center', alignItems: 'center' }}>
                    <Image source={require('./images/logo.png')} style={styles.logo} />
                    <Text style={styles.title}>Login</Text>
                </View>
                <View style={{ justifyContent: 'flex-start', alignItems: 'center' }}>
                    <MyInput title='Username/ Email' />
                    <MyInput title='Password' />
                </View>
                <View style={{ justifyContent:'center', alignItems:'center'}}>
                    <MyButton title='Masuk' bgColor='#3EC6FF' color='#FFFFFF' />
                    <Text style={{ fontSize: 24, color: '#3EC6FF' }}>atau</Text>
                    <MyButton title='Daftar ?' bgColor='#003366' color='#FFFFFF' />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "space-around"
    },
    logo: {
        marginTop: 63
    },
    title: {
        fontSize: 24,
        color: '#003366',
        margin: 20,
        padding: 20
    },
});